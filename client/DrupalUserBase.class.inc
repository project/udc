<?php
// $Id$
/**
 * @file
 * -----------------------------------------------------------------------------
 * Drupal 7 DP UserConnector module - External script client base class
 * -----------------------------------------------------------------------------
 * @author Stefan Wilhelm
 * @package org.drupal.module.dpuser_connector
 * @license GPL2
 */

if (defined('DRUPAL_ROOT') && !class_exists('DPUserConnectorTestCaseBase')) {
    throw new Exception("DP UserConnector Module client classes cannot be included in Drupal context (it makes no sense anyway).");
}

/**
 * Implements the base functionality to establish and test the connection to the
 * server script, as well as sending a request with arguments. Uses basic sockets
 * for HTTP connections or cURL for HTTPS connections. To send the basic request
 * to the server ( protected sendRequest() ), request data are converted into
 * JSON format, and the server script response payload data are JSON parsed/unserialized.
 * Additional status, error and debugging data are provided with the response.
 *
 * @throws Exception
 */
class DrupalUserBase {

    /**
     * The security token to be sent and matched
     * @var string
     */
    private static $serverToken = '';

    /**
     * Defines if SSL has to be used for the local connection (HTTPS)
     * @var bool
     */
    private static $serverUseSSL = TRUE;

    /**
     * Contains the site name you want to refer to. This is the name
     * of the subfolder in the DRUPAL_ROOT/sites directory, not necessarily
     * the domain name/host name.
     * @var string
     */
    private static $serverSite = '';

    /**
     * Contains the server script process status
     * @var int
     */
    private $serverResponseStatus = NULL;

    /**
     * Contains the server script process time
     * @var double
     */
    private $serverProcessTime = NULL;

    /**
     * Contains extended debug information send by the server script
     * Only available if debugging switched on.
     * @var mixed
     */
    private $serverDebug = NULL;

    /**
     * Contains the error send by the server script
     * @var string
     */
    private $serverError = '';

    /**
     * Returns if the request shall be sent using SSL (requires cURL)
     * @return bool
     */
    public static function getUseSSL() {
        return self::$serverUseSSL;
    }

    /**
     * Sets if the request shall be sent using SSL (requires cURL)
     * @return bool
     */
    public static function setUseSSL($ssl) {
        self::$serverUseSSL = (bool) $ssl;
    }

    /**
     * Sets the site you want to refer to. This is the name of the subfolder
     * in the DRUPAL_ROOT/sites directory, not necessarily the host name.
     * @param string $sitename
     */
    public static function setSite($sitename) {
        self::$serverSite = trim($sitename);
    }

    /**
     * Returns the site name referred to
     * @see setSite
     * @return string
     */
    public static function getSite() {
        return self::$serverSite;
    }

    /**
     * Returns the number of millisecones the server script needed for execution
     * @return string
     */
    public function getServerProcessTime() {
        return $this->serverProcessTime;
    }

    /**
     * Returns error and debug information sent with the response
     * @return string
     */
    public function getDebug() {
        return $this->serverDebug;
    }

    /**
     * Returns server error information sent with the response
     * @return string
     */
    public function getError() {
        return $this->serverError;
    }

    /**
     * Sets the server token
     * @param string $serverScriptToken
     */
    public static function setToken($serverScriptToken) {
        self::$serverToken = (string) $serverScriptToken;
    }

    /**
     * Constructor
     * @return DrupalUserAuth
     */
    protected function __construct() {
    }

    /**
     * Send a ping request to check if the server script is up and the token
     * is correct. Returns TRUE on success, FALSE on fail. The error message
     * can be obtained with $this->getError();
     * @throws Exception
     * @return bool
     */
    public function ping() {
        try {
            $this->sendRequest('ping');
            $err = $this->getError();
            return empty($err);
        } catch (Exception $e) {
            return FALSE;;
        }
    }

    /**
     * Send the request to the server script and update the instance variables
     * of this object as return values.
     * @param string $request
     * @param array $data = array
     * @throws Exception
     */
    protected function sendRequest($request, array $data=array()) {
        if (!is_array($data)) {
            throw new Exception('Drupal user request data must be an assoc. array');
        }
        $data = http_build_query(array(
            'request' => $request,
            'site' => self::$serverSite,
            'token' => strval(self::$serverToken),
            'data' => $data
        ));
        $host = $_SERVER['HTTP_HOST'];
        $addr = $_SERVER['SERVER_ADDR'];
        $path = dirname(dirname(str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__))) . '/server/server.php';

        if (self::$serverUseSSL) {
            if (!function_exists('curl_init')) {
                throw new Exception("You need cURL to use SSL connections");
            }
            else {
                // SSL --> Curl
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_HEADER => TRUE,
                    CURLOPT_URL => "https://$host/$path",
                    CURLOPT_POSTFIELDS => $data,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_SSL_VERIFYPEER => FALSE,
                    CURLOPT_CONNECTTIMEOUT => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_USERAGENT => 'dp-user-connector-client',
                    CURLOPT_FORBID_REUSE => TRUE,
                    CURLOPT_NOPROGRESS => TRUE,
                ));
                $r = curl_exec($curl);
                curl_close($curl);
            }
        }
        else {
            // No SSL --> Socket
            $fp = fsockopen("$addr", 80, $errno, $errstr, 5);
            if (!$fp) {
                throw new Exception("Cannot check user: $errstr");
            }
            else {
                fputs($fp, "POST $path HTTP/1.1\r\n");
                fputs($fp, "Host: $host\r\n");
                fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
                fputs($fp, "Content-length: " . strlen($data) . "\r\n");
                fputs($fp, "Connection: close\r\n\r\n");
                fputs($fp, $data);
                $r = '';
                while (!feof($fp)) {
                    $r .= fgets($fp, 128);
                }
            }
        }

        // Parse the response
        $r_string = str_replace("\r\n", "\n", $r);
        $r = explode("\n", $r_string);
        if (strpos($r[0], '200 OK') === FALSE) {
            if (strpos($r[0], '401') !== FALSE) {
                throw new Exception("Cannot check user: Server refused connection, check your script token.");
            }
            else {
                throw new Exception("Cannot check user: Server returned {$r[0]} for request $addr/$path");
            }
        }
        else {
            while (!empty($r) && reset($r) != '') {
                array_shift($r);
            }

            if (!empty($r)) array_shift($r);

            if (empty($r)) {
                throw new Exception("Cannot check user: Server did not text data");
            }
            else {
                $response = @json_decode(reset($r), TRUE);
                if (!is_array($response)) {
                    $this->serverDebug = $r_string;
                    throw new Exception('Server response format incorrect. $this->debug contains the response text.');
                }
                else {
                    $this->serverResponseStatus = $response['status'];
                    $this->serverProcessTime = isset($response['processtime']) ? $response['processtime'] : NULL;
                    $this->serverDebug = isset($response['debug']) ? $response['debug'] : NULL;
                    $this->serverError = isset($response['error']) ? $response['error'] : NULL;
                    return is_array($response['data']) ? $response['data'] : array();;
                }
            }
        }
        return FALSE;
    }
}
