<?php
// $Id$
/**
 * @file
 *
 * Example common include file, used to set the server token, include
 * required classes and define auxillary functions.
 *
 */

require_once('../client/DrupalUserAuth.class.inc');
require_once('../client/DrupalUserInfo.class.inc');
require_once('../client/DrupalUserList.class.inc');

DrupalUserBase::setToken('change-this-to-the-token-in-your-module-config');

// Timing functions
static $_timer = 0;
function tic() { global $_timer; $_timer = microtime(true); }
function toc() { global $_timer; return (microtime(true) - $_timer) * 1000.0; @flush(); }
