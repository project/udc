<?php
// $Id$
/**
 * @file
 * -----------------------------------------------------------------------------
 * Drupal 7 - DP UserConnector connector module - configuration
 * -----------------------------------------------------------------------------
 *
 * @author Stefan Wilhelm (stfwi)
 * @package org.drupal.project.dpuser_connector
 * @license GPL2
 */


/**
 * Configuration autocomplete callback for user names
 * @param string $string
 * @return string
 */
function dpuser_connector_users_autocomplete($string='') {
    $tags = drupal_explode_tags($string);
    $string = trim(array_pop($tags));
    $matches = array();
    if ($string) {
        $r = db_select('users')->fields('users', array('name'))->condition('name', db_like($string) . '%', 'LIKE')->range(0, 10)->execute();
        $p = !empty($tags) ? (implode(', ', $tags) . ', ') : ('');
        foreach ($r as $v) {
            if (!in_array($v->name, $tags)) $matches[$p . $v->name] = check_plain($v->name);
        }
    }
    drupal_json_output($matches);
}

/**
 * Configuration autocomplete callback for role names
 * @param string $string
 * @return string
 */
function dpuser_connector_roles_autocomplete($string='') {
    $tags = drupal_explode_tags($string);
    $string = trim(array_pop($tags));
    $matches = array();
    if ($string) {
        $r = db_select('role')->fields('role', array('name'))->condition('name', db_like($string) . '%', 'LIKE')->range(0, 10)->execute();
        $p = !empty($tags) ? (implode(', ', $tags) . ', ') : ('');
        foreach ($r as $v) {
            if (strtolower($v->name) != 'anonymous user' && strtolower($v->name) != 'authenticated user') {
                if (!in_array($v->name, $tags)) {
                    $matches[$p . $v->name] = check_plain($v->name);
                }
            }
        }
    }
    drupal_json_output($matches);
}

/**
 * Configuration autocomplete callback for user fields/profile fields
 * @param string $string
 * @return string
 */
function dpuser_connector_userfields_autocomplete($string='') {
    $tags = drupal_explode_tags($string);
    $string = trim(array_pop($tags));
    $matches = array();
    if ($string) {
        if (strpos($string, '<') === 0) {
            $matches['<all>'] = check_plain('<all>');
        }
        else {
            $p = !empty($tags) ? (implode(', ', $tags) . ', ') : ('');
            if (db_table_exists('profile_field')) {
                $r = db_select('profile_field')->fields('profile_field', array('name'))->condition('name', db_like($string) . '%', 'LIKE')->range(0, 10)->execute();
                foreach ($r as $v) {
                    if (!in_array($v->name, $tags)) $matches[$p . $v->name] = check_plain($v->name);
                }
            }
            if (db_table_exists('field_config_instance')) {
                $r = db_select('field_config_instance')
                    ->fields('field_config_instance', array('field_name'))
                    ->condition('field_name', db_like($string) . '%', 'LIKE')
                    ->condition('entity_type', 'user', '=')
                    ->range(0, 10)->execute();
                foreach ($r as $k => $v) {
                    if (!in_array($v->field_name, $tags)) $matches[$p . $v->field_name] = check_plain($v->field_name);
                }
            }
        }
    }
    drupal_json_output($matches);
}


/**
 * Returns the configuration form structure
 * @return array
 */
function dpuser_connector_admin() {
    include_once(dirname(__FILE__) . '/admin.txt.inc');
    $token = DpUserConnectorConfig::instance()->getToken();
    if (empty($token)) $token = str_replace('=', '', base64_encode(sha1(print_r($_SERVER, TRUE))));
    $unlisted_users = DpUserConnectorConfig::instance()->getUnlistedUsers();
    $unlisted_users = is_array($unlisted_users) ? drupal_implode_tags($unlisted_users) : '';
    $profile_fields = DpUserConnectorConfig::instance()->getListedProfileFields();
    $profile_fields = ($profile_fields === TRUE) ? '<all>' : (is_array($profile_fields) ? drupal_implode_tags($profile_fields) : '');
    $roles = DpUserConnectorConfig::instance()->getListedRoles();
    $roles = is_array($roles) ? drupal_implode_tags($roles) : '';
    $has_ssl = function_exists('curl_init');

    $form['dpuser_connector_intro'] = array(
        '#markup' => dpuser_connector_admin_get_form_introduction()
    );
    $form['dpuser_connector_token'] = array(
        '#type' => 'textfield',
        '#title' => t('Token'),
        '#default_value' => $token,
        '#size' => 100,
        '#maxlength' => 200,
        '#description' => t('The security token used to identify your external script request.
                             This is required because more than one virtual host can be located
                             on the same server with the same IP address. The server script will
                             not accept an empty string here (use some big random string, the module
                             creates one for you at startup).  The client classes provide the static
                             function <code>DrupalUserBase::setToken($string)</code> set the token
                             used for the request. You can also enter the token string in a text
                             file and specify here <i><b>file:</b>./path/to/file</i>, where the path
                             can be absolute (leading "/") or relative to $_SERVER[\'DOCUMENT_ROOT\'].
                             Note that the file must not be accessible via HTTP.'),
        '#required' => TRUE,
    );
    $form['dpuser_connector_listed_roles'] = array(
        '#type' => 'textfield',
        '#title' => t('Allowed roles'),
        '#description' => t('Here you can restrict the access using the user roles. If you enter roles
                            here, only users with these roles will be accepted/listed by the server script.
                            Leave blank to accept all roles.'),
        '#size' => 100,
        '#maxlength' => 5000,
        '#autocomplete_path' => 'admin/config/people/dpuser_connector/autocomplete/roles',
        '#default_value' => $roles
    );
    $form['dpuser_connector_unlisted_users'] = array(
        '#type' => 'textfield',
        '#title' => t('Unlisted users'),
        '#description' => t('If there are users you don\'t want to be exported, like "admin" etc, specify
                            them here and the server will exclude these accounts from the response.'),
        '#size' => 100,
        '#maxlength' => 5000,
        '#autocomplete_path' => 'admin/config/people/dpuser_connector/autocomplete/users',
        '#default_value' => $unlisted_users
    );
    $form['dpuser_connector_listed_profile_fields'] = array(
        '#type' => 'textfield',
        '#title' => t('Allowed profile fields'),
        '#description' => t('You can restrict the profile values that the server script returns by listing
                            here the allowed fields. If you want no fields, leave the text blank. If you want
                            no resrictions, enter "<i><b>&lt;all&gt;</b></i>". Ther server script does not return
                            an error to the client if surpressed fields are requested, it just does not include
                            them in the response.'),
        '#size' => 100,
        '#maxlength' => 5000,
        '#autocomplete_path' => 'admin/config/people/dpuser_connector/autocomplete/userfields',
        '#default_value' => $profile_fields
    );
    $form['dpuser_connector_require_https'] = array(
        '#type' => 'checkbox',
        '#title' => t('SSL only'),
        '#description' => ($has_ssl ? '' : ( '<span style="color:#ff7777;">' . t('Your PHP does not support
                            the cURL library, unfortunately you cannot enable SSL.') . '</span> ') )
                        . t('Specifies if the server script accepts only connections via HTTPS. The server
                            script will then reject HTTP connections with an error message. Note that you need
                            the cURL library to enable this feature.  The client classes provide the static function
                            <code>DrupalUserBase::setUseSSL($bool)</code> to switch the connection type.'),
        '#default_value' => $has_ssl ? DpUserConnectorConfig::instance()->getRequireHttps() : FALSE,
        '#disabled' => !$has_ssl
    );
    $form['dpuser_connector_enable_debug'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable server script debugging information'),
        '#description' => t('Enables the server script to send additional debug information with the response. The
                            data can be fetched from the client using <code>$result = $instance->getDebug()</code>,
                            where <code>$instance</code> is an object derived from <code>DrupalUserBase</code>.'),
        '#default_value' => DpUserConnectorConfig::instance()->getDebugEnabled()
    );
    $form['dpuser_connector_additional'] = array(
        '#markup' => dpuser_connector_admin_get_form_additional()
    );
    return system_settings_form($form);
}


/**
 * Form validation callback
 * @param array $form
 * @param array &$form_state
 * @return void
 */
function dpuser_connector_admin_validate($form, &$form_state) {
    $token = &$form_state['values']['dpuser_connector_token'];
    $https = &$form_state['values']['dpuser_connector_require_https'];
    $debug = &$form_state['values']['dpuser_connector_enable_debug'];
    $unlisted_users = &$form_state['values']['dpuser_connector_unlisted_users'];
    $fields = &$form_state['values']['dpuser_connector_listed_profile_fields'];
    $roles = &$form_state['values']['dpuser_connector_listed_roles'];

    $token = trim($token, " \t");
    $https = (bool) $https;
    $debug = (bool) $debug;
    $unlisted_users = trim($unlisted_users, " \t,");
    $unlisted_users = empty($unlisted_users) ? array() : drupal_explode_tags($unlisted_users);
    $fields = trim($fields, " \t,");
    $fields = empty($fields) ? array() : (strtolower($fields) == '<all>' ? TRUE : drupal_explode_tags($fields));
    $roles = trim($roles, " \t,");
    $roles = empty($roles) ? array() : drupal_explode_tags($roles);
    if (empty($token)) {
        form_set_error('dpuser_connector_token', t('The server script will not accept an empty token.'));
    }
    if ($https && !function_exists('curl_init')) {
        form_set_error('dpuser_connector_require_https', t('PHP has no cURL library, SSL cannot be used.'));
        $https = FALSE;
    }
}
