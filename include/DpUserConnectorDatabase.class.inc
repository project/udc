<?php
// $Id$
/**
 * @file
 * -----------------------------------------------------------------------------
 * Drupal 7 - DP UserConnector connector module - server script database access
 * -----------------------------------------------------------------------------
 *
 * @author Stefan Wilhelm (stfwi)
 * @package org.drupal.project.dpuser_connector
 * @license GPL2
 */

require_once( dirname(__FILE__) . '/DpUserConnectorConfig.class.inc');

/**
 * Provider class for Drupal database lookups. Includes filtering and data
 * conditioning.
 *
 * @throws Exception
 */
class DpUserConnectorDatabase {

    /**
     * Stores all available roles as key/value pair (role id => role name).
     * @var array
     */
    public $availableRoles = array();

    /**
     * Stores the available user profile fields as key value pair: fid => name.
     * @var array
     */
    public $availableProfileFields = array();

    /**
     * Stores the available user fields as key value pair: id => field_name.
     * @var array
     */
    public $availableUserFields = array();

    /**
     * Contains the allowed RIDs for IN_SET queries
     * @var array
     */
    public $allowedRoleIds = array();


    /**
     * Contains the lowercase unlisted users
     * @var array
     */
    public $unlistedUsers = array();


    /**
     * Constructor
     */
    public function __construct() {
        // All roles
        $r = db_query('SELECT rid, name FROM {role}');
        foreach ($r as $v) {
            $this->availableRoles[$v->rid] = $v->name;
        }

        // Listed role ids
        $lr = DpUserConnectorConfig::instance()->getListedRoles();
        if (!empty($lr) && $lr !== TRUE) {
            foreach ($lr as $r => $v) {
                $lr[$r] = strtolower($v);
            }
            foreach ($this->availableRoles as $r => $v) {
                if (in_array(strtolower($v), $lr)) {
                    $this->allowedRoleIds[$r] = $r;
                }
            }
        }

        // Unlisted users
        $this->unlistedUsers = DpUserConnectorConfig::instance()->getUnlistedUsers();
        foreach ($this->unlistedUsers as $r => $v) {
            $this->unlistedUsers[$r] = trim(strtolower($v));
        }
    }


    /**
     * If filter is an array, the method removes all profile and user fields which
     * are not in the $filter list. If TRUE, no fields are removed. if FALSE, string
     * or object, all fields are removed.
     * @param array $filter
     */
    public function loadAvailableFields($filter) {
        if (is_array($filter) && empty($filter) || !is_array($filter) && !$filter) {
            $this->availableUserFields = array();
            $this->availableProfileFields = array();
            return;
        }
        else {
            $ls = DpUserConnectorConfig::instance()->getListedProfileFields();

            // Initialize user fields
            $this->availableUserFields = array();
            $r = db_query("SELECT id, field_name FROM {field_config_instance} WHERE entity_type='user'");
            foreach ($r as $k => $v) {
                if (($ls===TRUE || in_array($v->field_name, $ls))) {
                    $this->availableUserFields[$v->id] = $v;
                }
            }

            // Initialize available profile fields for the case profile is used
            if (db_table_exists('profile_field') && db_table_exists('profile_value')) {
                $r = db_query('SELECT fid, name, type FROM {profile_field}');
                foreach ($r as $v) {
                    if (($ls===TRUE || in_array($v->name, $ls))) {
                        $this->availableProfileFields[$v->fid] = $v;
                    }
                }
            }

            // Filter
            if (is_array($filter)) {
                foreach ($filter as $k => $v) $filter[$k] = trim(strtolower($v));
                $newList = array();
                // User fields
                foreach ($this->availableUserFields as $k => $v) {
                    if (in_array(strtolower($v->field_name), $filter)) {
                        $newList[$k] = $v;
                    }
                }
                $this->availableUserFields = $newList;

                // Profile fields
                $newList = array();
                foreach ($this->availableProfileFields as $k => $v) {
                    if (in_array(strtolower($v->name), $filter)) {
                        $newList[$k] = $v;
                    }
                }
                $this->availableProfileFields = $newList;
            }
        }
    }

    /**
     * Returns the roles of the user
     * @param int $uid
     */
    public function getUserRoles($uid) {
        $roles = $this->availableRoles;
        $uid = intval($uid);
        if ($uid > 0) {
            $r = db_query('SELECT rid FROM {users_roles} WHERE uid=:uid', array(':uid' => $uid));
            $rr = array();
            foreach ($r as $v) {
                $rr[] = $roles[$v->rid];
            }
        }
        return $rr;
    }

    /**
     * Returns the user profile fields. If the $fields parameter is TRUE, all
     * allowed fields are listed, otherwise an array with the machine names
     * of the fields has to be specified. If $fields==FALSE, the method returns
     * an empty array. If a field cannot be fetched rapidly, a full drupal
     * bootstrap is performed (with corresponding performance losses).
     * @param type $uid
     * @return array
     */
    public function getUserFields($uid) {
        if (empty($this->availableUserFields)) {
            return $this->getUserProfileFields($uid);
        }
        else {
            return $this->getUserFieldsUserLoad($uid);
        }
    }

    /**
     * Returns the user profile fields. If the $fields parameter is TRUE, all
     * allowed fields are listed, otherwise an array with the machine names
     * of the fields has to be specified. If $fields==FALSE, the method returns
     * an empty array. Note that only fields can be exported here which provide
     * the table field "<field_name>_value".
     * @param type $uid
     * @return array
     */
    public function getUserProfileFields($uid) {
        $pfields = &$this->availableProfileFields;
        $rr = array();
        if (!empty($pfields)) {
            $r = db_query("SELECT fid, value FROM {profile_value} WHERE uid=:uid AND fid :inset", array('uid' => $uid, ':inset' => array_keys($pfields)));
            foreach ($r as $v) {
                // Known types
                switch ($pfields[$v->fid]->type) {
                    case 'date':
                        $v->value = unserialize($v->value);
                        break;
                    default:
                }
                $rr[$pfields[$v->fid]->name] = $v->value;
            }
            return $rr;
        }
    }

    /**
     * Returns user profile and common fields using Drupal user_load().
     * @param type $uid
     * @return array
     */
    public function getUserFieldsUserLoad($uid) {
        if (!function_exists('user_load')) {
            drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
            header('Content-Type: text/plain');
            set_error_handler('checkuser_error_handler', E_ALL | E_STRICT);
            set_exception_handler('checkuser_exception_handler');
        }

        $user = user_load($uid);
        $rr = array();
        foreach ($this->availableProfileFields as $v) {
            $v = $v->name;
            if (isset($user->$v)) $rr[$v] = $user->$v;
        }
        foreach ($this->availableUserFields as $v) {
            $v = $v->field_name;
            if (isset($user->$v)) $rr[$v] = $user->$v;
        }
        return $rr;
    }
}
