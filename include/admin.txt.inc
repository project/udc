<?php
// $Id$
/**
 * @file
 * -----------------------------------------------------------------------------
 * Drupal 7 - DP UserConnector connector module - configuration/help texts
 * -----------------------------------------------------------------------------
 *
 * @author Stefan Wilhelm (stfwi)
 * @package org.drupal.project.dpuser_connector
 * @license GPL2
 */


/**
 * Returns highlighted PHP code of a given file path
 * @param string $file
 */
function dpuser_connector_admin_get_highlighted_source($file) {
    @ob_start();
    @highlight_string(@file_get_contents($file));
    return '<br/><br/><div style="background-color: #eee;">' . @ob_get_clean() . '</div>';
}


/**
 * Parses and formats the README file (only once) into an array and returns the
 * specified section text.
 * @param string $section
 * @return string
 */
function dpuser_connector_admin_get_readme_section($section) {
    static $help = NULL;
    if (!is_array($help)) {
        $help = array();
        if (is_file(dirname(dirname(__FILE__)) . '/README')) {
            $readme = htmlspecialchars(file_get_contents(dirname(dirname(__FILE__)) . '/README'), ENT_NOQUOTES);
            $readme = trim(str_replace("\t", '   ', str_replace("\r\v", '', $readme)), " \n");
            $readme = preg_replace('/((http|https|ftp):\/\/[\w\/\.]+)/i', '<a href="$1">$1</a>', $readme);
            $readme = explode("\n", $readme);
            $text = $sect = '';
            while (!empty($readme)) {
                $line = array_shift($readme);
                $linet= trim($line);
                if (drupal_strlen($linet) == 0) { // empty returns FALSE on e.g. '0', so strlen
                    continue;
                }
                elseif ($line[0] !== ' ') {
                    if (!empty($sect)) {
                        $text .= '</p>';
                        $text = preg_replace('/[\s]*<p>[\s]*<\/p>[\s]*/i', '', $text);
                        if (!empty($text)) $help[strtolower($sect)] = $text;
                    }
                    $text = '<p>';
                    $sect = $linet;
                }
                else {
                    if (strpos($linet, '<a href=') === 0) {
                        $text .= "<ul><li>$linet</li></ul>";
                    }
                    elseif ($linet[0] == '*') {
                        $ul = '';
                        $li = trim($linet, '* ');
                        while (!empty($readme)) {
                            $linet = htmlspecialchars(trim(array_shift($readme)));
                            if ($linet == '') {
                                break;
                            }
                            elseif ($linet[0] == '*') {
                                $ul .= "<li>$li</li>";
                                $li = trim($linet, '* ');
                            }
                            else {
                                $li .= ' ' . $linet;
                            }
                        }
                        if (!empty($li)) $ul .= "<li>$li</li>";
                        $text .= "</p><ul>$ul</ul><p>";
                        unset($ul, $li);
                    }
                    elseif ($linet[strlen($linet)-1] == '.') {
                        $text .= "$linet</p><p>";
                    }
                    else {
                        $text .= "$linet ";
                    }
                }
            }
            $text .= '</p>';
            $text = preg_replace('/\<p\>[\s]*\<\/p\>/i', '', $text);

            if (!empty($sect)) {
                $help[strtolower($sect)] = trim($text, "\n\t ");
            }
        }
    }

    if ($section == '*') {
        return $help;
    }
    elseif (isset($help[strtolower($section)])) {
        return "\n\n<h1>$section</h1>\n" . $help[strtolower($section)];
    }
    else {
        return '';
    }
}

/**
 * Returns the introduction text for the configutation page
 * @return string
 */
function dpuser_connector_admin_get_form_introduction() { ob_start(); ?>

Here you can specify the settings of the server script. The full help is provided
on the <a href="<? print base_path() ?>admin/help/dpuser_connector">help page</a>.

<?php return ob_get_clean(); }


/**
 * Returns the additional information text for the configutation page
 * @return string
 */
function dpuser_connector_admin_get_form_additional() { ob_start(); ?>
<?php return ob_get_clean(); }


/**
 * Returns the help text for the help page
 * @return string
 */
function dpuser_connector_admin_get_help() {
    drupal_add_css('
        div.dpuc-help       { margin-bottom: 20pt; padding-bottom: 20pt; border-bottom: solid 1px black; }
        div.dpuc-help h1    { }
        div.dpuc-help h2    { margin-top: 30pt; }
        div.dpuc-help h3    { margin-top: 30pt; }
        div.dpuc-help p     { text-align: justify; }
        div.dpuc-help ul    { margin-left: 30pt;}
        div.dpuc-help ul li { margin-bottom: 3pt; }
        ', array('type' => 'inline'));

    $help = '<div class="dpuc-help">';

    $help .= dpuser_connector_admin_get_readme_section('Summary');
    $help .= dpuser_connector_admin_get_readme_section('Requirements');
    $help .= dpuser_connector_admin_get_readme_section('FAQ');
    $help .= dpuser_connector_admin_get_readme_section('Troubleshooting');
    $help .= dpuser_connector_admin_get_readme_section('Maintainers');

    if (is_file(dirname(dirname(__FILE__)) . '/CHANGELOG')) {
        $help .= '<h1>Changelog</h1>';
        $help .= '<pre>' . @file_get_contents(dirname(dirname(__FILE__)) . '/CHANGELOG') . '</pre>';
    }

    $help .= '<h1>Examples</h1>';
    $help .= 'Here some examples in place. You can find the exactly these files in the directory <i>modules/dpuser_connector/examples</i>.';

    $help .= '<h3>Ping the server to check the connection and token</h3>';
    $help .= 'You can use <code>ping</code> with all <code>DrupalUser*****</code> classes,
              as the feature is implemented in <code>DrupalUserBase</code> and the other
              classes are derived from this class.
              ';
    $help .= dpuser_connector_admin_get_highlighted_source(dirname(dirname(__FILE__)) . '/examples/ping.php');

    $help .= '<h3>Request user authentification</h3>';
    $help .= 'This is an example for a HTTPS basic authentification using the <code>DrupalUserAuth</code>
              class. If your (external) script is invoked quite often, you should save the response user
              data in the session instead of sending a local authentification request everytime. The reason
              is not only the additional localhost HTTP connection, but the simple fact that calculating
              the SHA256 checksum takes Drupal comaratively much processing time.
              ';
    $help .= dpuser_connector_admin_get_highlighted_source(dirname(dirname(__FILE__)) . '/examples/userauth.php');


    $help .= '<h3>Request single user information</h3>';
    $help .= 'If you want to get information about a user, but don\'t want to check the password,
              use the <code>DrupalUserInfo</code> class. The server script response will contain the
              same information as the <code>DrupalUserAuth</code> class response - except that the
              password check is skipped and the <code>valid</code> property is <code>TRUE</code> if
              the user exists.
              ';
    $help .= dpuser_connector_admin_get_highlighted_source(dirname(dirname(__FILE__)) . '/examples/userinfo.php');


    $help .= '<h3>Request a user list</h3>';
    $help .= 'There is a variety of situations where is necessary to get a simple ist of users from the
              Drupal database. The method of getting the data using DP UserConnector is the <code>DrupalUserList</code>
              class:';
    $help .= dpuser_connector_admin_get_highlighted_source(dirname(dirname(__FILE__)) . '/examples/userlist.php');

    $help .= '</div>';
    return $help;
}
